﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UvaProjeto.Domain.Shared.Entities;
using UvaProjeto.ViewModels;

namespace UvaProjeto.AutoMapperConfig
{
    public class ApplicationProfile : Profile
    {
        public ApplicationProfile()
        {
            CreateMap<Aluno, AlunoViewModel>().ReverseMap();
            CreateMap<Campus, CampusViewModel>().ReverseMap();
            CreateMap<Curso, CursoViewModel>().ReverseMap();

        }
    }
}
