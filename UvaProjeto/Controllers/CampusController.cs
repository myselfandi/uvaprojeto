﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UvaProjeto.Application.ApplicationServicesInterfaces;
using UvaProjeto.Domain.Shared.Entities;
using UvaProjeto.ViewModels;

namespace UvaProjeto.Controllers
{
    public class CampusController : Controller
    {
        private readonly ICampusApplicationService _campusApplicationService;
        private readonly IMapper _mapper;
        public CampusController(ICampusApplicationService campusApplicationService, IMapper mapper)
        {
            _campusApplicationService = campusApplicationService;
            _mapper = mapper;
        }

        // GET: Campus
        public ActionResult Index()
        {
            var campuss = _campusApplicationService.GetAll();
            var campussViewModel = _mapper.Map<ICollection<CampusViewModel>>(campuss);
            return View(campussViewModel);
        }

        // GET: Campus/Details/5
        public ActionResult Details(Guid id)
        {
            var campus = _campusApplicationService.GetById(id);
            var campusViewModel = _mapper.Map<CampusViewModel>(campus);
            return View(campusViewModel);
        }

        // GET: Campus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Campus/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CampusViewModel campusViewModel)
        {
            try
            {
                var campus = _mapper.Map<Campus>(campusViewModel);
                _campusApplicationService.Add(campus);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return View();
            }
        }

        // GET: Campus/Edit/5
        public ActionResult Edit(Guid id)
        {
            var campus = _campusApplicationService.GetById(id);
            var campusViewModel = _mapper.Map<CampusViewModel>(campus);
            return View(campusViewModel);
        }

        // POST: Campus/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CampusViewModel campusViewModel)
        {
            try
            {
                var campus = _mapper.Map<Campus>(campusViewModel);
                _campusApplicationService.Update(campus);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Campus/Delete/5
        public ActionResult Delete(Guid id)
        {
            var campus = _campusApplicationService.GetById(id);
            var campusViewModel = _mapper.Map<CampusViewModel>(campus);
            return View(campusViewModel);
        }

        // POST: Campus/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Guid id, IFormCollection collection)
        {
            try
            {
                var campus = _campusApplicationService.GetById(id);
                _campusApplicationService.Remove(campus);
                return RedirectToAction(nameof(Index));
            }
            catch(Exception e)
            {
                Console.Write(e);
                return View();
            }
        }
    }
}