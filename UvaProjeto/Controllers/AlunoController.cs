﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UvaProjeto.Application.ApplicationServicesInterfaces;
using UvaProjeto.Domain.Shared.Entities;
using UvaProjeto.Domain.Shared.ExtensionMethods;
using UvaProjeto.ViewModels;
using System.Diagnostics;

namespace UvaProjeto.Controllers
{
    public class AlunoController : Controller
    {
        // GET: Aluno
        private readonly IAlunoApplicationService _alunoApplicationService;
        private readonly ICampusApplicationService _campusApplicationService;
        private readonly ICursoApplicationService _cursoApplicationService;
        private readonly IMapper _mapper;

        public AlunoController(IAlunoApplicationService alunoApplicationService, 
            ICampusApplicationService campusApplicationService,
            ICursoApplicationService cursoApplicationService,
            IMapper mapper)
        {
            _alunoApplicationService = alunoApplicationService;
            _campusApplicationService = campusApplicationService;
            _cursoApplicationService = cursoApplicationService;
            _mapper = mapper;
        }
        // GET: Aluno
        public ActionResult Index()
        {
            Stopwatch sw = Stopwatch.StartNew();
            var alunos = _alunoApplicationService.GetAll();
            sw.Stop();

            ViewBag.TempoExecucao_Read = sw.Elapsed.TotalMilliseconds;

            var alunosViewModel = _mapper.Map<IEnumerable<AlunoViewModel>>(alunos);
            return View(alunosViewModel);
        }

        // GET: Aluno/Details/5
        public ActionResult Details(Guid id)
        {
            var aluno = _alunoApplicationService.GetById(id);
            var alunoViewModel = _mapper.Map<AlunoViewModel>(aluno);
            return View(alunoViewModel);
        }

        // GET: Aluno/Create
        public ActionResult Create()
        {
            ViewBag.Campus = _campusApplicationService.GetAll();
            ViewBag.Cursos = _cursoApplicationService.GetAll();
            return View();
        }

        // POST: Aluno/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AlunoViewModel alunoViewModel)
        {
            try
            {
                var aluno = _mapper.Map<Aluno>(alunoViewModel);
                if (aluno.ModeloValido())
                {
                    Stopwatch sw = Stopwatch.StartNew();
                    _alunoApplicationService.Add(aluno);
                    sw.Stop();
                    ViewBag.TempoExecucao_Write = sw.Elapsed.TotalMilliseconds;
                    return View("Index", _mapper.Map<IEnumerable<AlunoViewModel>>(_alunoApplicationService.GetAll()));
                }
                else
                {
                    ViewBag.Campus = _campusApplicationService.GetAll();
                    ViewBag.Cursos = _cursoApplicationService.GetAll();
                    ViewBag.Notifications = aluno.Notifications.ToStringExtended();
                    return View();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                return View();
            }
        }

        // GET: Aluno/Edit/5
        public ActionResult Edit(Guid id)
        {
            ViewBag.Campus = _campusApplicationService.GetAll();
            ViewBag.Cursos = _cursoApplicationService.GetAll();
            var aluno = _alunoApplicationService.GetById(id);
            var alunoViewModel = _mapper.Map<AlunoViewModel>(aluno);
            return View(alunoViewModel);
        }

        // POST: Aluno/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AlunoViewModel alunoViewModel)
        {
            try
            {
                var aluno = _mapper.Map<Aluno>(alunoViewModel);
                _alunoApplicationService.Update(aluno);
                return RedirectToAction(nameof(Index));
            }
            catch(Exception e)
            {
                Console.Write(e);
                return View();
            }
        }

        // GET: Aluno/Delete/5
        public ActionResult Delete(Guid id)
        {
            var aluno = _alunoApplicationService.GetById(id);
            var alunoViewModel = _mapper.Map<AlunoViewModel>(aluno);
            return View(alunoViewModel);
        }

        // POST: Aluno/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Guid id, IFormCollection collection)
        {
            try
            {
                var aluno = _alunoApplicationService.GetById(id);
                _alunoApplicationService.Remove(aluno);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}