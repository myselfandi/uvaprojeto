﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UvaProjeto.Application.ApplicationServicesInterfaces;
using UvaProjeto.Domain.Shared.Entities;
using UvaProjeto.ViewModels;

namespace UvaProjeto.Controllers
{
    public class CursoController : Controller
    {
        private readonly ICursoApplicationService _cursoApplicationService;
        private readonly IMapper _mapper;
        public CursoController(ICursoApplicationService cursoApplicationService, IMapper mapper)
        {
            _cursoApplicationService = cursoApplicationService;
            _mapper = mapper;
        }

        // GET: Curso
        public ActionResult Index()
        {
            var cursos = _cursoApplicationService.GetAll();
            var cursosViewModel = _mapper.Map<IEnumerable<CursoViewModel>>(cursos);
            return View(cursosViewModel);
        }

        // GET: Curso/Details/5
        public ActionResult Details(Guid id)
        {
            var curso = _cursoApplicationService.GetById(id);
            var cursoViewModel = _mapper.Map<CursoViewModel>(curso);
            return View(cursoViewModel);
        }

        // GET: Curso/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Curso/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CursoViewModel cursoViewModel)
        {
            try
            {
                var curso = _mapper.Map<Curso>(cursoViewModel);
                _cursoApplicationService.Add(curso);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Curso/Edit/5
        public ActionResult Edit(Guid id)
        {
            var curso = _cursoApplicationService.GetById(id);
            var cursoViewModel = _mapper.Map<CursoViewModel>(curso);
            return View(cursoViewModel);
        }

        // POST: Curso/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CursoViewModel cursoViewModel)
        {
            try
            {
                var curso = _mapper.Map<Curso>(cursoViewModel);
                _cursoApplicationService.Update(curso);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Curso/Delete/5
        public ActionResult Delete(Guid id)
        {
            var curso = _cursoApplicationService.GetById(id);
            var cursoViewModel = _mapper.Map<CursoViewModel>(curso);
            return View(cursoViewModel);
        }

        // POST: Curso/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(CursoViewModel cursoViewModel)
        {
            try
            {
                var curso = _mapper.Map<Curso>(cursoViewModel);
                _cursoApplicationService.Remove(curso);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}