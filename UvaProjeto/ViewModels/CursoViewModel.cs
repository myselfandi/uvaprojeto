﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UvaProjeto.Domain.Shared.Entities;
using UvaProjeto.Domain.Shared.ValueObjects;

namespace UvaProjeto.ViewModels
{
    public class CursoViewModel
    {
        public Guid Id { get; set; }

        public string Nome { get; set; }
    }
}
