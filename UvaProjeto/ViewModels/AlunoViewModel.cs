﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UvaProjeto.Domain.Shared.Entities;
using UvaProjeto.Domain.Shared.ValueObjects;

namespace UvaProjeto.ViewModels
{
    public class AlunoViewModel
    {
        public AlunoViewModel() => Id = Guid.NewGuid();

        public Guid Id { get; set; }

        public Nome Nome { get; set; }

        [Display(Name = "Endereço")]
        public Endereco Endereco { get; set; }

        [Display(Name = "Matrícula")]
        public int Matricula { get; set; }

        public string CPF { get; set; }

        [Display(Name = "Data de Nascimento")]
        public DateTime DataNascimento { get; set; }

        [Display(Name = "Id do Campus")]
        public Guid CampusId { get; set; }

        [Display(Name = "Id do Curso")]
        public Guid CursoId { get; set; }

        public virtual Campus Campus { get; set; }

        public virtual Curso Curso { get; set; }
    }
}
