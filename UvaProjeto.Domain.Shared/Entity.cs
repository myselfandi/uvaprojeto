﻿using Flunt.Notifications;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UvaProjeto.Domain.Shared
{
    public abstract class Entity : Notifiable
    {
        #region Constructors
        public Entity() => Id = Guid.NewGuid();
        #endregion

        #region Properties
        [Key]
        public Guid Id { get; private set; }
        public DateTime DataCadastro { get; set; }
        #endregion

        #region Methods
        public abstract bool ModeloValido();
        #endregion
    }
}
