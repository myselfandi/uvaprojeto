﻿using Flunt.Notifications;
using System;
using System.Collections.Generic;
using System.Text;

namespace UvaProjeto.Domain.Shared.ExtensionMethods
{
    public static class NotificationsExtensionMethods
    {
        public static string ToStringExtended(this IReadOnlyCollection<Notification> notifications)
        {
            var notificationsString = "";
            foreach (var notification in notifications)
            {
                notificationsString += notification.Message + Environment.NewLine; 
            }
            return notificationsString;
        }
    }
}
