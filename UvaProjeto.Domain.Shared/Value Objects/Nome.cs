﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UvaProjeto.Domain.Shared.ValueObjects
{
    public class Nome : ValueObject
    {
        public Nome(string primeiroNome, string sobrenome)
        {
             PrimeiroNome = primeiroNome;
             Sobrenome = sobrenome;
        }

        public Nome() { }

        [Display(Name="Primeiro Nome")]
        public string PrimeiroNome { get; set; }
        public string Sobrenome { get; set; }
    }
}
