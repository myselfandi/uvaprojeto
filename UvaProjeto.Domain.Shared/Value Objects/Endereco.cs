﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UvaProjeto.Domain.Shared.ValueObjects
{
    public class Endereco : ValueObject
    {
        public Endereco(string cep, string logradouro, Int64 numero, string bloco, string complemento)
        {
            CEP = cep;
            Logradouro = logradouro;
            Numero = numero;
            Bloco = bloco;
            Complemento = complemento;
        }

        public Endereco() { }

        public string CEP { get; set; }
        public string Logradouro { get; set; }
        public Int64 Numero { get; set; }
        public string Bloco { get; set; }
        public string Complemento { get; set; }
    }
}
