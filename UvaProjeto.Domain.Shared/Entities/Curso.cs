﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UvaProjeto.Domain.Shared.Entities
{
    public class Curso : Entity
    {
        public Curso()
        {
            Alunos = new List<Aluno>();
        }

        [Display(Name = "Curso")]
        public string Nome { get; set; }

        public virtual ICollection<Aluno> Alunos { get; set; }

        public override bool ModeloValido()
        {
            return true;
        }
    }
}
