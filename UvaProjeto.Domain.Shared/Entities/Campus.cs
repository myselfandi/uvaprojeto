﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using UvaProjeto.Domain.Shared.ValueObjects;

namespace UvaProjeto.Domain.Shared.Entities
{
    public class Campus : Entity
    {
        public Campus()
        {
            Alunos = new List<Aluno>();
        }

        [Display(Name="Campus")]
        public string Nome { get; set; }

        public virtual Endereco Endereco { get; set; }

        public virtual ICollection<Aluno> Alunos { get; set; }

        public override bool ModeloValido()
        {
            return true;
        }
    }
}
