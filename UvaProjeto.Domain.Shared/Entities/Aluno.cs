﻿using Flunt.Validations;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using UvaProjeto.Domain.Shared.ValueObjects;

namespace UvaProjeto.Domain.Shared.Entities
{
    public class Aluno : Entity
    {

        public virtual Nome Nome { get; set; }
        public virtual Endereco Endereco { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Matricula { get; set; }
        public string CPF { get; set; }
        public DateTime DataNascimento { get; set; }
        public Guid CampusId { get; set; }
        public Guid CursoId { get; set; }

        public virtual Campus Campus { get; set; }
        public virtual Curso Curso { get; set; }

        #region Domain notifications
        public override bool ModeloValido()
        {
            if (Nome.PrimeiroNome.Length <= 3)
            {
                AddNotification("Nome.PrimeiroNome" , "O primeiro nome deve ter no mínimo 3 dígitos.");
            }
            if (Nome.Sobrenome.Length <= 3)
            {
                AddNotification("Nome.PrimeiroNome", "O primeiro nome deve ter no mínimo 3 dígitos.");
            }
            return Notifications.Count == 0;
        }
        #endregion

        #region Design By Contracts
        public bool ModeloValido2()
        {
            AddNotifications(new Contract()
                .Requires()
                .HasMinLen(Nome.PrimeiroNome, 3, "Nome.PrimeiroNome", "O primeiro nome deve ter no mínimo 3 dígitos.")
                .HasMinLen(Nome.Sobrenome, 3, "Nome.Sobrenome", "O sobrenome deve ter no mínimo 3 dígitos.")
            );
            return Valid;
        }
        #endregion
    }
}
