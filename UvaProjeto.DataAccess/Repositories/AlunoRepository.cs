﻿using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.DataAccess.Context;
using UvaProjeto.Domain.RepositoriesInterfaces;
using UvaProjeto.Domain.Shared.Entities;

namespace UvaProjeto.DataAccess.Repositories
{
    public class AlunoRepository : RepositoryBase<Aluno>, IAlunoRepository
    {
        public AlunoRepository(EntityFrameworkContext context) : base(context)
        {
            
        }

        // Enquanto não sai a correção do entity frameworks, é necessário fazer desta forma para atualizar os value objects
        public override void Update(Aluno aluno)
        {
            var entry = _context.Attach(aluno);
            var endereco = _context.Entry(aluno.Endereco);
            var nome = _context.Entry(aluno.Nome);
            endereco.OriginalValues.SetValues(endereco.GetDatabaseValues()); // gets home address
            nome.OriginalValues.SetValues(nome.GetDatabaseValues());
            entry.OriginalValues.SetValues(entry.GetDatabaseValues()); // gets student
            _context.SaveChanges();
        }
    }
}
