﻿using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.DataAccess.Context;
using UvaProjeto.Domain.RepositoriesInterfaces;
using UvaProjeto.Domain.Shared.Entities;

namespace UvaProjeto.DataAccess.Repositories
{
    public class CursoRepository : RepositoryBase<Curso>, ICursoRepository
    {
        public CursoRepository(EntityFrameworkContext context) : base(context)
        {
        }
    }
}
