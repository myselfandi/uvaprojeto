﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.DataAccess.Context;
using UvaProjeto.Domain.RepositoriesInterfaces;
using UvaProjeto.Domain.Shared.Entities;
using UvaProjeto.Domain.Shared.ValueObjects;

namespace UvaProjeto.DataAccess.Repositories
{
    public class CampusRepository : RepositoryBase<Campus>, ICampusRepository
    {
        public CampusRepository(EntityFrameworkContext context, IMapper mapper) : base(context)
        {
            _mapper = mapper;
        }

        private readonly IMapper _mapper;

        // <NETFIX>
        //      Enquanto não sai a correção do entity frameworks, é necessário fazer desta forma para atualizar os value objects
        // </NETFIX>
        public override void Update(Campus campus)
        {
            var entry = _context.Attach(campus);
            var adEntry = _context.Entry(campus.Endereco);
            adEntry.OriginalValues.SetValues(adEntry.GetDatabaseValues()); 
            entry.OriginalValues.SetValues(entry.GetDatabaseValues());
            _context.SaveChanges();
        }
    }
}
