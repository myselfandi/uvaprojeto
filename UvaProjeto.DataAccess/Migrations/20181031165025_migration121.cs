﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace UvaProjeto.DataAccess.Migrations
{
    public partial class migration121 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "DataNascimento",
                table: "Aluno",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "DataNascimento",
                table: "Aluno",
                nullable: true,
                oldClrType: typeof(DateTime));
        }
    }
}
