﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using UvaProjeto.DataAccess.Context;

namespace UvaProjeto.DataAccess.Migrations
{
    [DbContext(typeof(EntityFrameworkContext))]
    [Migration("20181030190211_migration11")]
    partial class migration11
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("UvaProjeto.Domain.Shared.Entities.Aluno", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CPF");

                    b.Property<Guid>("CampusId");

                    b.Property<Guid>("CursoId");

                    b.Property<DateTime>("DataCadastro");

                    b.Property<string>("DataNascimento");

                    b.Property<int>("Matricula")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.HasKey("Id");

                    b.HasIndex("CampusId");

                    b.HasIndex("CursoId");

                    b.ToTable("Aluno");
                });

            modelBuilder.Entity("UvaProjeto.Domain.Shared.Entities.Campus", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DataCadastro");

                    b.Property<string>("Nome");

                    b.HasKey("Id");

                    b.ToTable("Campus");
                });

            modelBuilder.Entity("UvaProjeto.Domain.Shared.Entities.Curso", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DataCadastro");

                    b.Property<string>("Nome");

                    b.HasKey("Id");

                    b.ToTable("Curso");
                });

            modelBuilder.Entity("UvaProjeto.Domain.Shared.Entities.Aluno", b =>
                {
                    b.HasOne("UvaProjeto.Domain.Shared.Entities.Campus", "Campus")
                        .WithMany("Alunos")
                        .HasForeignKey("CampusId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("UvaProjeto.Domain.Shared.Entities.Curso", "Curso")
                        .WithMany("Alunos")
                        .HasForeignKey("CursoId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.OwnsOne("UvaProjeto.Domain.Shared.ValueObjects.Nome", "Nome", b1 =>
                        {
                            b1.Property<Guid?>("AlunoId");

                            b1.Property<string>("PrimeiroNome");

                            b1.Property<string>("Sobrenome");

                            b1.ToTable("Aluno");

                            b1.HasOne("UvaProjeto.Domain.Shared.Entities.Aluno")
                                .WithOne("Nome")
                                .HasForeignKey("UvaProjeto.Domain.Shared.ValueObjects.Nome", "AlunoId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });

                    b.OwnsOne("UvaProjeto.Domain.Shared.ValueObjects.Endereco", "Endereco", b1 =>
                        {
                            b1.Property<Guid>("AlunoId");

                            b1.Property<string>("Bloco");

                            b1.Property<string>("CEP");

                            b1.Property<string>("Complemento");

                            b1.Property<string>("Logradouro");

                            b1.Property<long>("Numero");

                            b1.ToTable("Aluno");

                            b1.HasOne("UvaProjeto.Domain.Shared.Entities.Aluno")
                                .WithOne("Endereco")
                                .HasForeignKey("UvaProjeto.Domain.Shared.ValueObjects.Endereco", "AlunoId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });
                });

            modelBuilder.Entity("UvaProjeto.Domain.Shared.Entities.Campus", b =>
                {
                    b.OwnsOne("UvaProjeto.Domain.Shared.ValueObjects.Endereco", "Endereco", b1 =>
                        {
                            b1.Property<Guid>("CampusId");

                            b1.Property<string>("Bloco");

                            b1.Property<string>("CEP");

                            b1.Property<string>("Complemento");

                            b1.Property<string>("Logradouro");

                            b1.Property<long>("Numero");

                            b1.ToTable("Campus");

                            b1.HasOne("UvaProjeto.Domain.Shared.Entities.Campus")
                                .WithOne("Endereco")
                                .HasForeignKey("UvaProjeto.Domain.Shared.ValueObjects.Endereco", "CampusId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });
                });
#pragma warning restore 612, 618
        }
    }
}
