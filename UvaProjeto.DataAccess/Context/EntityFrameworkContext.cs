﻿using Flunt.Notifications;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UvaProjeto.Domain.Shared.Entities;

namespace UvaProjeto.DataAccess.Context
{
    public class EntityFrameworkContext : DbContext
    {
        #region Constructors
        public EntityFrameworkContext(DbContextOptions<EntityFrameworkContext> options) : base(options)
        {
        }
        #endregion

        #region Dataset Definitions
        public DbSet<Aluno> Aluno { get; set; }
        public DbSet<Curso> Curso { get; set; }
        public DbSet<Campus> Campus { get; set; }
        #endregion

        #region Methods
        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("DataCadastro") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("DataCadastro").CurrentValue = DateTime.Now;
                }
                else if (entry.State == EntityState.Modified)
                {
                    entry.Property("DataCadastro").IsModified = false;
                }
            }
            return base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Aluno>().OwnsOne(c => c.Nome);
            modelBuilder.Entity<Aluno>().OwnsOne(c => c.Endereco);
            modelBuilder.Entity<Campus>().OwnsOne(c => c.Endereco);
            modelBuilder.Ignore<Notification>();
        }
        #endregion
    }
}
