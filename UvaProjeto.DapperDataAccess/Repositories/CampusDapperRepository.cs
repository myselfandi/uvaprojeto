﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using UvaProjeto.Domain.RepositoriesInterfaces;
using UvaProjeto.Domain.Shared.Entities;
using UvaProjeto.Domain.Shared.ValueObjects;

namespace UvaProjeto.DapperDataAccess
{
    public class CampusDapperRepository : DapperBaseRepository<Campus>, ICampusRepository
    {
        public CampusDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public override void Add(Campus obj)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<Campus> GetAll()
        {
            IEnumerable<Campus> queryResult = new List<Campus>();
            DbConnect(conn =>
            {
                var query = $@"
                    SELECT
                        CAMP.ID CAMPUS_ID, 
                        CAMP.NOME CAMPUS_NOME, 
                        CAMP.ENDERECO_CEP, 
                        CAMP.ENDERECO_LOGRADOURO, 
                        CAMP.ENDERECO_NUMERO, 
                        CAMP.ENDERECO_BLOCO, 
                        CAMP.ENDERECO_COMPLEMENTO
                    FROM {_owner}.CAMPUS CAMP
                    ";

                queryResult = conn.Query<Campus, Endereco, Campus>(query, (campus, endereco) => 
                    {                     
                        campus.Endereco = endereco;
                        return campus;
                    }, splitOn: "ENDERECO_CEP").AsList();
            });
            return queryResult;
        }

        public override Campus GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public override void Remove(Campus obj)
        {
            throw new NotImplementedException();
        }

        public override void Update(Campus obj)
        {
            throw new NotImplementedException();
        }
    }
}
