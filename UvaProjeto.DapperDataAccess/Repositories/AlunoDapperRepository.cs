﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using UvaProjeto.Domain.RepositoriesInterfaces;
using UvaProjeto.Domain.Shared.Entities;
using UvaProjeto.Domain.Shared.ValueObjects;

namespace UvaProjeto.DapperDataAccess
{
    public class AlunoDapperRepository : DapperBaseRepository<Aluno>, IAlunoRepository
    {
        public AlunoDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public override void Add(Aluno obj)
            {
            DbConnect(conn =>
            {
                //VALUES(CONVERT(datetime, '{DateTime.Now}'), '{obj.CPF}', CONVERT(datetime, '{obj.DataNascimento}'), '{obj.Nome.PrimeiroNome}', '{obj.Nome.Sobrenome}', '{obj.Endereco.CEP}', '{obj.Endereco.Logradouro}', '{obj.Endereco.Numero}', '{obj.Endereco.Bloco}', '{obj.Endereco.Complemento}', '{obj.CampusId}', '{obj.CursoId}');
                var now = DateTime.Now.ToString("yyyy-MM-dd");
                var command = $@"
                    INSERT INTO {_owner}.Aluno (Id, DataCadastro, CPF, DataNascimento, Nome_PrimeiroNome, Nome_Sobrenome, Endereco_CEP, Endereco_Logradouro, Endereco_Numero, Endereco_Bloco, Endereco_Complemento, CampusId, CursoId)
                       VALUES(@Id, @DataCadastro, @CPF, @DataNascimento, @Nome_PrimeiroNome, @Nome_Sobrenome, @Endereco_CEP, @Endereco_Logradouro, @Endereco_Numero, @Endereco_Bloco, @Endereco_Complemento, @CampusId, @CursoId)
                ";
                var commandResult = conn.Execute(command, new {
                    obj.Id,
                    DataCadastro = DateTime.Now,
                    obj.CPF,
                    obj.DataNascimento,
                    Nome_PrimeiroNome = obj.Nome.PrimeiroNome,
                    Nome_Sobrenome = obj.Nome.Sobrenome,
                    Endereco_CEP = obj.Endereco.CEP,
                    Endereco_Logradouro = obj.Endereco.Logradouro,
                    Endereco_Numero = obj.Endereco.Numero,
                    Endereco_Bloco = obj.Endereco.Bloco,
                    Endereco_Complemento = obj.Endereco.Complemento,
                    obj.CampusId,
                    obj.CursoId
                });
            });
        }

        public override IEnumerable<Aluno> GetAll()
        {
            IEnumerable<Aluno> queryResult = new List<Aluno>();
            DbConnect(conn =>
            {
                var query = $@"
                    SELECT
                        ALUN.ID, 
                        ALUN.DATACADASTRO,
                        ALUN.MATRICULA, 
                        ALUN.CPF, 
                        ALUN.DATANASCIMENTO, 
                        ALUN.NOME_PRIMEIRONOME, 
                        ALUN.NOME_SOBRENOME, 
                        ALUN.ENDERECO_CEP, 
                        ALUN.ENDERECO_LOGRADOURO, 
                        ALUN.ENDERECO_NUMERO, 
                        ALUN.ENDERECO_BLOCO, 
                        ALUN.ENDERECO_COMPLEMENTO,
                        ALUN.CAMPUSID CAMPUS_ID, 
                        CAMP.NOME CAMPUS_NOME, 
                        ALUN.CURSOID CURSO_ID, 
                        CURS.NOME CURSO_NOME
                    FROM {_owner}.Aluno alun
                    INNER JOIN {_owner}.CAMPUS CAMP
                        ON ALUN.CAMPUSID = CAMP.ID
                    INNER JOIN {_owner}.CURSO CURS
                        ON ALUN.CURSOID = CURS.ID
                    ";

                //queryResult = conn.Query<Aluno>(query).AsList();
                queryResult = conn.Query<Aluno, Nome, Endereco, Campus, Curso, Aluno>(query, (aluno, nome, endereco, campus, curso) => 
                    { 
                        aluno.Nome = nome;
                        aluno.Endereco = endereco;
                        aluno.Campus = campus;
                        aluno.Curso = curso;
                        return aluno;
                    }, splitOn: "NOME_PRIMEIRONOME, ENDERECO_CEP, CAMPUS_ID, CURSO_ID").AsList();
            });
            return queryResult;
        }

        public override Aluno GetById(Guid id)
        {
            Aluno queryResult = new Aluno();
            DbConnect(conn =>
            {
                var query = $@"
                    SELECT
                        ALUN.ID, 
                        ALUN.DATACADASTRO,
                        ALUN.MATRICULA, 
                        ALUN.CPF, 
                        ALUN.DATANASCIMENTO, 
                        ALUN.NOME_PRIMEIRONOME, 
                        ALUN.NOME_SOBRENOME, 
                        ALUN.ENDERECO_CEP, 
                        ALUN.ENDERECO_LOGRADOURO, 
                        ALUN.ENDERECO_NUMERO, 
                        ALUN.ENDERECO_BLOCO, 
                        ALUN.ENDERECO_COMPLEMENTO,
                        ALUN.CAMPUSID CAMPUS_ID, 
                        CAMP.NOME CAMPUS_NOME, 
                        ALUN.CURSOID CURSO_ID, 
                        CURS.NOME CURSO_NOME
                    FROM {_owner}.Aluno alun
                    INNER JOIN {_owner}.CAMPUS CAMP
                        ON ALUN.CAMPUSID = CAMP.ID
                    INNER JOIN {_owner}.CURSO CURS
                        ON ALUN.CURSOID = CURS.ID
                    WHERE ALUN.ID = '{id}'
                    ";

                queryResult = conn.Query<Aluno, Nome, Endereco, Campus, Curso, Aluno>(query, (aluno, nome, endereco, campus, curso) =>
                {
                    aluno.Nome = nome;
                    aluno.Endereco = endereco;
                    aluno.Campus = campus;
                    aluno.Curso = curso;
                    return aluno;
                }, splitOn: "NOME_PRIMEIRONOME, ENDERECO_CEP, CAMPUS_ID, CURSO_ID").FirstOrDefault();
            });
            return queryResult;
        }

        public override void Remove(Aluno obj)
        {
            throw new NotImplementedException();
        }

        public override void Update(Aluno obj)
        {
            throw new NotImplementedException();
        }
    }
}
