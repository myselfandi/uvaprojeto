﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using UvaProjeto.Domain.RepositoriesInterfaces;

namespace UvaProjeto.DapperDataAccess
{
    public abstract class DapperBaseRepository<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        #region variables
        private static string _connectionString;
        protected readonly string _owner;
        protected readonly string _entity;
        #endregion

        #region Constructos
        public DapperBaseRepository(IConfiguration iConfiguration)
        {
            _connectionString = _connectionString ?? iConfiguration.GetConnectionString("UvaProjeto");
            _owner = "DBO";
        }
        #endregion

        #region Utility Method
        protected readonly Action<Action<SqlConnection>> DbConnect = (action) =>
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                action(conn);
            }
        };
        #endregion

        #region Methods
        public abstract void Add(TEntity obj);
        public abstract IEnumerable<TEntity> GetAll();
        public abstract TEntity GetById(Guid id);
        public abstract void Remove(TEntity obj);
        public abstract void Update(TEntity obj);

        public void Dispose()
        {
        }
        #endregion
    }
}
