﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.Domain.Shared.Entities;

namespace UvaProjeto.DapperDataAccess.Dapper_Maps
{
    public class CampusMap : EntityMap<Campus>
    {
        public CampusMap()
        {
            Map(c => c.Id).ToColumn("CAMPUS_ID");
            Map(c => c.Nome).ToColumn("CAMPUS_NOME");
        }
    }
}
