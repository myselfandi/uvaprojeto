﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.Domain.Shared.Entities;

namespace UvaProjeto.DapperDataAccess.Dapper_Maps
{
    public class AlunoMap : EntityMap<Aluno>
    {
        public AlunoMap()
        {
            Map(c => c.CampusId).ToColumn("CAMPUS_ID");
            Map(c => c.CursoId).ToColumn("CURSO_ID");
        }
    }
}
