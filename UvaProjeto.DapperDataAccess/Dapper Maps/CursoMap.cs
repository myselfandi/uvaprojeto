﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.Domain.Shared.Entities;

namespace UvaProjeto.DapperDataAccess.Dapper_Maps
{
    public class CursoMap : EntityMap<Curso>
    {
        public CursoMap()
        {
            Map(c => c.Id).ToColumn("CURSO_ID");
            Map(c => c.Nome).ToColumn("CURSO_NOME");
        }
    }
}
