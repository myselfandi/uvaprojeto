﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.Domain.Shared.ValueObjects;

namespace UvaProjeto.DapperDataAccess.Dapper_Maps.Value_Objects_Maps
{
    public class EnderecoMap : EntityMap<Endereco>
    {
        public EnderecoMap()
        {
            Map(e => e.CEP).ToColumn("ENDERECO_CEP");
            Map(e => e.Logradouro).ToColumn("ENDERECO_LOGRADOURO");
            Map(e => e.Numero).ToColumn("ENDERECO_NUMERO");
            Map(e => e.Bloco).ToColumn("ENDERECO_BLOCO");
            Map(e => e.Complemento).ToColumn("ENDERECO_COMPLEMENTO");
        }
    }
}
