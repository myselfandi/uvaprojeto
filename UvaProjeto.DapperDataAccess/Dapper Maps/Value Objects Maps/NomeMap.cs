﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.Domain.Shared.ValueObjects;

namespace UvaProjeto.DapperDataAccess.Dapper_Maps.Value_Objects_Maps
{
    public class NomeMap : EntityMap<Nome>
    {
        public NomeMap()
        {
            Map(n => n.PrimeiroNome).ToColumn("NOME_PRIMEIRONOME");
            Map(n => n.Sobrenome).ToColumn("NOME_SOBRENOME");
        }
    }
}
