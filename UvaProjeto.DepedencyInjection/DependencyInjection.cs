﻿using Microsoft.Extensions.DependencyInjection;
using UvaProjeto.Application.ApplicationServices;
using UvaProjeto.Application.ApplicationServicesInterfaces;
using UvaProjeto.DataAccess.Repositories;
using UvaProjeto.Domain.DomainServices;
using UvaProjeto.Domain.DomainServicesInterfaces;
using UvaProjeto.Domain.RepositoriesInterfaces;
using UvaProjeto.DapperDataAccess;

namespace UvaProjeto.CrossCutting
{
    public class DependencyInjection
    {
        public static void InjectInto(IServiceCollection services)
        {
            #region Aluno
            services.AddScoped<IAlunoApplicationService, AlunoApplicationService>();
            services.AddScoped<IAlunoService, AlunoService>();
            services.AddScoped<IAlunoRepository, AlunoRepository>();
            #endregion

            #region Campus
            services.AddScoped<ICampusApplicationService, CampusApplicationService>();
            services.AddScoped<ICampusService, CampusService>();
            services.AddScoped<ICampusRepository, CampusRepository>();
            #endregion

            #region Curso
            services.AddScoped<ICursoApplicationService, CursoApplicationService>();
            services.AddScoped<ICursoService, CursoService>();
            services.AddScoped<ICursoRepository, CursoRepository>();
            #endregion
        }
    }
}
