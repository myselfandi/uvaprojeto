﻿using Dapper.FluentMap;
using System;
using UvaProjeto.DapperDataAccess.Dapper_Maps;
using UvaProjeto.DapperDataAccess.Dapper_Maps.Value_Objects_Maps;

namespace UvaProjeto.CrossCutting
{
    public class RegistrationTools
    {
        public static void DapperMaps()
        {
            # region Value Objects
            FluentMapper.Initialize(config => config.AddMap(new NomeMap()));
            FluentMapper.Initialize(config => config.AddMap(new EnderecoMap()));
            #endregion

            #region Entities
            FluentMapper.Initialize(config => config.AddMap(new AlunoMap()));
            FluentMapper.Initialize(config => config.AddMap(new CursoMap()));
            FluentMapper.Initialize(config => config.AddMap(new CampusMap()));
            #endregion
        }
    }
}
