﻿using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.Application.ApplicationServicesInterfaces;
using UvaProjeto.Domain.DomainServicesInterfaces;
using UvaProjeto.Domain.Shared.Entities;

namespace UvaProjeto.Application.ApplicationServices
{
    public class CursoApplicationService : ApplicationServiceBase<Curso>, ICursoApplicationService
    {
        private readonly ICursoService _cursoService;
        public CursoApplicationService(ICursoService cursoService) : base (cursoService)
        {
            _cursoService = cursoService;
        }
    }
}
