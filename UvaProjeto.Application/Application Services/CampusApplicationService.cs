﻿using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.Application.ApplicationServicesInterfaces;
using UvaProjeto.Domain.DomainServicesInterfaces;
using UvaProjeto.Domain.Shared.Entities;

namespace UvaProjeto.Application.ApplicationServices
{
    public class CampusApplicationService : ApplicationServiceBase<Campus>, ICampusApplicationService
    {
        private readonly ICampusService _campusService;
        public CampusApplicationService(ICampusService campusService) : base (campusService)
        {
            _campusService = campusService;
        }
    }
}
