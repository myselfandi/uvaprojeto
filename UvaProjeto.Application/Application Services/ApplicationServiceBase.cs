﻿using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.Application.ApplicationServicesInterfaces;
using UvaProjeto.Domain.DomainServicesInterfaces;

namespace UvaProjeto.Application.ApplicationServices
{
    public class ApplicationServiceBase<TEntity> : IApplicationServiceBase<TEntity> where TEntity : class
    {
        #region Properties
        private readonly IServiceBase<TEntity> _serviceBase;
        #endregion

        #region Construtors
        public ApplicationServiceBase(IServiceBase<TEntity> serviceBase) => _serviceBase = serviceBase;
        #endregion

        #region Methods
        public void Add(TEntity obj) => _serviceBase.Add(obj);
        public IEnumerable<TEntity> GetAll() => _serviceBase.GetAll();
        public TEntity GetById(Guid id) => _serviceBase.GetById(id);
        public void Remove(TEntity obj) => _serviceBase.Remove(obj);
        public void Update(TEntity obj) => _serviceBase.Update(obj);
        #endregion
    }
}
