﻿using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.Application.ApplicationServicesInterfaces;
using UvaProjeto.Domain.DomainServicesInterfaces;
using UvaProjeto.Domain.Shared.Entities;

namespace UvaProjeto.Application.ApplicationServices
{
    public class AlunoApplicationService : ApplicationServiceBase<Aluno>, IAlunoApplicationService
    {
        private readonly IAlunoService _alunoService;
        public AlunoApplicationService(IAlunoService alunoService) : base (alunoService)
        {
            _alunoService = alunoService;
        }
    }
}
