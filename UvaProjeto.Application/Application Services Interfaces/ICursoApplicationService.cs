﻿using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.Domain.Shared.Entities;

namespace UvaProjeto.Application.ApplicationServicesInterfaces
{
    public interface ICursoApplicationService : IApplicationServiceBase<Curso>
    {
    }
}
