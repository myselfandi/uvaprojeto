﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UvaProjeto.Application.ApplicationServicesInterfaces
{
    public interface IApplicationServiceBase<TEntity> where TEntity : class
    {
        /// <summary>
        /// Adiciona um aluno no banco de dados.
        /// </summary>
        /// <param name="Aluno">Objeto "Aluno"</param>
        /// <remarks>Validar se o e-mail informado é válido.</remarks>
        void Add(TEntity obj);

        /// <summary>
        /// Recupera um registro de aluno de acordo com seu identificador
        /// </summary>
        /// <param name="Id">Id do aluno</param>
        /// <remarks></remarks>
        TEntity GetById(Guid id);

        /// <summary>
        /// Recupera todos os alunos do banco de dados
        /// </summary>
        /// <remarks>Validar se o e-mail informado é válido.</remarks>
        IEnumerable<TEntity> GetAll();

        /// <summary>
        /// Atualiza um aluno no banco de dados.
        /// </summary>
        /// <param name="Aluno">Objeto "Aluno"</param>
        /// <remarks>Verificar se aluno existe no banco</remarks>
        void Update(TEntity obj);

        /// <summary>
        /// Remove um aluno no banco de dados.
        /// </summary>
        /// <param name="Aluno">Objeto "Aluno"</param>
        /// <remarks>Verificar se aluno existe no banco.</remarks>
        void Remove(TEntity obj);
    }
}
