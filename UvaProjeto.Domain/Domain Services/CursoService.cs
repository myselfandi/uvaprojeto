﻿using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.Domain.Shared.Entities;
using UvaProjeto.Domain.RepositoriesInterfaces;
using UvaProjeto.Domain.DomainServicesInterfaces;

namespace UvaProjeto.Domain.DomainServices
{
    public class CursoService : ServiceBase<Curso>, ICursoService
    {
        private readonly ICursoRepository _cursoRepository;
        public CursoService(ICursoRepository cursoRepository) : base(cursoRepository)
        {
            _cursoRepository = cursoRepository;
        }
    }
}
