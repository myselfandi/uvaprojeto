﻿using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.Domain.Shared.Entities;
using UvaProjeto.Domain.RepositoriesInterfaces;
using UvaProjeto.Domain.DomainServicesInterfaces;

namespace UvaProjeto.Domain.DomainServices
{
    public class CampusService : ServiceBase<Campus>, ICampusService
    {
        private readonly ICampusRepository _campusRepository;
        public CampusService(ICampusRepository campusRepository) : base(campusRepository)
        {
            _campusRepository = campusRepository;
        }
    }
}
