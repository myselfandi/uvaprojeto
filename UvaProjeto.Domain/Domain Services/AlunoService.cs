﻿using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.Domain.DomainServicesInterfaces;
using UvaProjeto.Domain.RepositoriesInterfaces;
using UvaProjeto.Domain.Shared.Entities;

namespace UvaProjeto.Domain.DomainServices
{
    public class AlunoService : ServiceBase<Aluno>, IAlunoService
    {
        private readonly IAlunoRepository _alunoRepository;
        public AlunoService(IAlunoRepository alunoRepository) : base(alunoRepository)
        {
            _alunoRepository = alunoRepository;
        }
    }
}
