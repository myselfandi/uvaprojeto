﻿using System;
using System.Collections.Generic;
using UvaProjeto.Domain.DomainServicesInterfaces;
using UvaProjeto.Domain.RepositoriesInterfaces;

namespace UvaProjeto.Domain.DomainServices
{
    public class ServiceBase<TEntity> : IDisposable, IServiceBase<TEntity> where TEntity : class
    {
        private readonly IRepositoryBase<TEntity> _respository;

        public ServiceBase(IRepositoryBase<TEntity> respository)
        {
            _respository = respository;
        }

        public void Add(TEntity obj)
        {
            _respository.Add(obj);
        }

        public void Dispose()
        {
            _respository.Dispose();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _respository.GetAll();
        }

        public TEntity GetById(Guid id)
        {
            return _respository.GetById(id);
        }

        public void Remove(TEntity obj)
        {
            _respository.Remove(obj);
        }

        public void Update(TEntity obj)
        {
            _respository.Update(obj);
        }
    }
}
