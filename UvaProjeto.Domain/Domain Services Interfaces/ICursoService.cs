﻿using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.Domain.Shared.Entities;

namespace UvaProjeto.Domain.DomainServicesInterfaces
{
    public interface ICursoService : IServiceBase<Curso>
    {
    }
}
