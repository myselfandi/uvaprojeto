﻿using System;
using System.Collections.Generic;
using System.Text;
using UvaProjeto.Domain.Shared.Entities;

namespace UvaProjeto.Domain.RepositoriesInterfaces
{
    public interface IAlunoRepository : IRepositoryBase<Aluno>
    {
    }
}
