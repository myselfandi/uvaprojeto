﻿using AutoMapper;

namespace Shared.Conversions
{
    // Automapper bool to int
    public class StringToBooleanToIntTypeConverter : ITypeConverter<bool, int>
    {
        public int Convert(bool source, int destination, ResolutionContext context)
        {
            if (!source)
                return 0;
            else
                return 1;
        }
    }
}
